FROM python
COPY hello.py /app/hello.py
COPY entrypoint.sh /entrypoint.sh
RUN pip install flask newrelic
RUN chmod + /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
EXPOSE 5000